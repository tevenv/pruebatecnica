import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioservicesService {

  constructor(private http:HttpClient) { }

  private url =environment.url
  private opts={
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getTipoDoc(){
    return this.http.get('assets/Data/tipdoc.json');
  }

  getUsuarios():Observable<any>{
    console.log('Este servicio');
    return this.http.get(this.url + 'usuario', this.opts);
  }

  getUsuario(id:number){
    return this.http.get(this.url+'usuario/'+id, this.opts)
  }

  crearUsuario(params:any){
    return this.http.post(this.url +'usuario',params, this.opts);
  }

  actualizarUsuario(params:any, id:number){
    return this.http.put(this.url+'usuario/'+ id,params, this.opts);
  }

  eliminarUsuario(id:number){
    return this.http.delete(this.url+'Usuario/'+id,this.opts)
  }

}
