import { Component, OnInit } from '@angular/core';
import { UsuarioservicesService } from './services/usuarioservices.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  usuario={
              npredio:1121702723001,
              usuario:"Brayan Antonio Carvajal Velaquez",
              tipid:1,
              numid:1121702723,
              telefono:3105892536,
              direccion:"calle falsa 123",
              email:"jhon.ruiz@yahoo.es"
          }  

  actuusuario={
                telefono:3158526954,
                direccion:"Calle siempre viva 123",
                email:""
              }
  
  public p:any[]=[];

  constructor(private usuarioserv:UsuarioservicesService){  }

  ngOnInit():void{
    console.log(this.usuario);
    
    console.log("Esto inicia aqui");
    

    
  }

  usuarios(){
    this.usuarioserv.getUsuarios().subscribe((res:any)=>{
      console.log(res);
      this.p.push(res)
      console.log(this.p);
      
    })
  }

  usuarioget(){
    this.usuarioserv.getUsuario(5).subscribe((res:any)=>{
      console.log(res);
      
    })
  }

  Crear(){
    this.usuarioserv.crearUsuario(this.usuario).subscribe((res:any)=>{
      console.log(res);
    })
  }

  actualizar(){
    this.usuarioserv.actualizarUsuario(this.actuusuario, 5).subscribe((res:any)=>{
      console.log(res);
    })
  }

  eliminar(){
    this.usuarioserv.eliminarUsuario(6).subscribe((res:any)=>{
      console.log(res);
    })
  }
}
