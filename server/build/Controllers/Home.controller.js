"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UC = void 0;
const sequelize_1 = require("sequelize");
const DB_config_1 = __importDefault(require("../DB.config"));
const usuario_query_1 = __importDefault(require("../QUERY/usuario.query"));
class UsuarioController {
    //consulta:queryInfo;
    //const sele:string;
    constructor() {
        /* this.consulta = new  queryInfo(); */
        //this.sele = this.consulta.selectgenral();
        //console.log(this.sele);
    }
    home(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            /* const consultar = consul.selectgenral();
            console.log(consultar); */
            // consul.insertar() , {type:QueryTypes.INSERT}
            // consul.actualizar('670723','1') , {type:QueryTypes.UPDATE}
            let respuesta;
            try {
                respuesta = yield DB_config_1.default.query(usuario_query_1.default.selectgenral(), { type: sequelize_1.QueryTypes.SELECT });
                console.log(respuesta);
                if (!respuesta[1]) {
                    respuesta = { Text: 'Sin Registros' };
                }
                //respuesta ={Text:'inserto bien'}
            }
            catch (error) {
                console.log(error);
                res.json({ Type: 'Error', Text: 'Texto de error' });
            }
            res.json(respuesta);
            //respuesta = Object.values(JSON.parse(JSON.stringify(respuesta)))
            console.log('Si funciona asi');
        });
    }
}
exports.UC = new UsuarioController();
