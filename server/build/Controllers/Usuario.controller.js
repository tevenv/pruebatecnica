"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UC = void 0;
const sequelize_1 = require("sequelize");
const DB_config_1 = __importDefault(require("../DB.config"));
const usuario_query_1 = __importDefault(require("../QUERY/usuario.query"));
class UsuarioController {
    //consulta:queryInfo;
    //const sele:string;
    // private respuesta:any={};
    constructor() {
    }
    Usuarios(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let respuesta;
            respuesta = { Text: 'Sin Registros' };
            try {
                respuesta = yield DB_config_1.default.query(usuario_query_1.default.selectgenral(), { type: sequelize_1.QueryTypes.SELECT });
                console.log(respuesta);
                if (!respuesta[0]) {
                    respuesta = { Text: 'Sin Registros' };
                }
            }
            catch (error) {
                console.log(error);
                res.json({ Type: 'Error', Text: 'Texto de error' });
            }
            res.json(respuesta);
        });
    }
    usuario(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let respuesta;
            try {
                respuesta = yield DB_config_1.default.query(usuario_query_1.default.selectusuario(req.params.id), { type: sequelize_1.QueryTypes.SELECT });
                if (!respuesta[0]) {
                    respuesta = { Text: 'Usuario no encontrado' };
                }
            }
            catch (error) {
                res.json({ Type: 'Error', Text: 'Se presento el siguiente error', Error: error });
            }
            res.json(respuesta);
        });
    }
    insertar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let respuesta;
            /* console.log(req); */
            // res.json(req)
            try {
                respuesta = yield DB_config_1.default.query(usuario_query_1.default.insertar(req.body), { type: sequelize_1.QueryTypes.INSERT });
                console.log(respuesta);
                if (!respuesta[0]) {
                    respuesta = { Text: 'Usuario no encontrado' };
                }
            }
            catch (error) {
                res.json({ Type: 'Error', Text: 'Se presento el siguiente error', Error: error });
            }
            res.json(respuesta);
        });
    }
    Actualizar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let respuesta;
            try {
                respuesta = yield DB_config_1.default.query(usuario_query_1.default.actualizar(req.body, req.params.id), { type: sequelize_1.QueryTypes.UPDATE });
                if (respuesta.length > 0) {
                    respuesta = { Text: 'Actualizado' };
                }
                else {
                    respuesta = { Text: 'No se pudo Actualizar' };
                }
            }
            catch (error) {
                res.json({ Type: 'Error', Text: 'Se presento el siguiente error', Error: error });
            }
            res.json(respuesta);
        });
    }
    eliminar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let respuesta;
            try {
                respuesta = yield DB_config_1.default.query(usuario_query_1.default.eliminar(req.params.id), { type: sequelize_1.QueryTypes.DELETE });
                console.log("Esto es respuesta", respuesta);
                respuesta = { Text: 'Usuario eliminado' };
            }
            catch (error) {
                res.json({ Type: 'Error', Text: 'Se presento el siguiente error', Error: error });
            }
            res.json(respuesta);
        });
    }
}
exports.UC = new UsuarioController();
