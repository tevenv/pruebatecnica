"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const Usuario_routes_1 = require("./Routes/Usuario.routes");
const Informacion_routes_1 = __importDefault(require("./Routes/Informacion.routes"));
class Server {
    //infor:Informacion;
    constructor() {
        this.app = (0, express_1.default)();
        this.usuario = new Usuario_routes_1.Usuario();
        //this.infor = new Informacion();
        this.config();
        this.rutas();
    }
    config() {
        //this.app.set('port', process.env.PORT|| 3000);
        this.app.use((0, morgan_1.default)('dev'));
        this.app.use((0, cors_1.default)());
        this.app.use(express_1.default.json());
        // this.app.use(bodyParser.json())
        this.app.use(express_1.default.urlencoded({ extended: false }));
    }
    rutas() {
        this.app.use('/usuario', this.usuario.config());
        this.app.use('/Formu', Informacion_routes_1.default);
    }
    start() {
        const puerto = process.env.PORT || 3000;
        this.app.listen(puerto, () => {
            console.log('Numero de puerto', puerto);
        });
        console.log("Esto ya esta funcionando");
    }
}
const server = new Server();
server.start();
