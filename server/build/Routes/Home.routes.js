"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Usuario = void 0;
const express_1 = require("express");
const Usuario_controller_1 = require("../Controllers/Usuario.controller");
class Usuario {
    constructor() {
        this.router = (0, express_1.Router)();
        //this.config()
    }
    config() {
        return this.router.get('/', Usuario_controller_1.UC.Usuarios);
    }
}
exports.Usuario = Usuario;
