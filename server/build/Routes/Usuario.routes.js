"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Usuario = void 0;
const express_1 = require("express");
const Usuario_controller_1 = require("../Controllers/Usuario.controller");
class Usuario {
    constructor() {
        this.router = (0, express_1.Router)();
        //this.config()
    }
    config() {
        return this.router.get('/', Usuario_controller_1.UC.Usuarios)
            .get('/:id', Usuario_controller_1.UC.usuario)
            .post('/', Usuario_controller_1.UC.insertar)
            .put('/:id', Usuario_controller_1.UC.Actualizar)
            .delete('/:id', Usuario_controller_1.UC.eliminar);
    }
}
exports.Usuario = Usuario;
