"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const keys_1 = __importDefault(require("./keys"));
const sequelize_1 = require("sequelize");
const conexion = new sequelize_1.Sequelize(keys_1.default.database.database, keys_1.default.database.user, keys_1.default.database.password, {
    host: "bu9skp8kzgprj95cxzne-mysql.services.clever-cloud.com",
    dialect: "mysql",
});
exports.default = conexion;
