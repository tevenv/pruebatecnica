import {Router} from "express"
import { json } from "sequelize/types";
import {UC} from '../Controllers/Usuario.controller'

export class Usuario{

    private router: Router 
    
    constructor( ){
        this.router = Router();
        
        //this.config()
    }

    config(){
        return this.router.get('/', UC.Usuarios)
        .get('/:id', UC.usuario)
        .post('/',UC.insertar)
        .put('/:id', UC.Actualizar)
        .delete('/:id', UC.eliminar);
    }

}