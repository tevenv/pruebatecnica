import{Sequelize, DataTypes} from 'sequelize';
import conexion from '../DB.config' 

/* const model={
    id:{
        type:DataTypes.INTEGER,
        primaryKey:true
    },
    Nombre:{
        type:DataTypes.STRING
    },
    Apellido:{
        type:DataTypes.STRING
    },
    Telefono:{
        type:DataTypes.STRING
    }
}

const name = 'usuario'

export const User = conexion.define(name,model,{tableName:name, timestamps:false}) */

export interface Usuario  {
    npredio:number,
    usuario:string,
    tipid:number,
    numid:number,
    telefono:number,
    direccion:string,
    email:string
}

