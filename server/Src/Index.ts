import express, { Application } from 'express'
import morgan from 'morgan'
import cors from 'cors'

import {Usuario} from './Routes/Usuario.routes'
import Informacion from './Routes/Informacion.routes'




class Server{

    app:Application;
    usuario:Usuario;
    
    //infor:Informacion;

    constructor(  ){
        this.app = express();
        this.usuario = new Usuario()
        //this.infor = new Informacion();
        this.config();
        this.rutas();
    }

    config(){
        //this.app.set('port', process.env.PORT|| 3000);
        this.app.use(morgan('dev'));
        this.app.use(cors());
        this.app.use(express.json());
        // this.app.use(bodyParser.json())
        this.app.use(express.urlencoded({extended:false}))
    }

    rutas(){
        this.app.use('/usuario',this.usuario.config());
        this.app.use('/Formu', Informacion);
    }

    start(){
        const puerto = process.env.PORT|| 3000
        this.app.listen(puerto,()=>{
            console.log('Numero de puerto', puerto);
            
        })
        console.log("Esto ya esta funcionando");
        
    }
}

const server = new Server();

server.start();