import{Request, Response } from 'express'
import { EmptyResultError, QueryTypes } from 'sequelize';
import conexion  from '../DB.config'
import { Usuario } from "../Models/Usuario.model";

import consul ,{ queryUsuario }  from '../QUERY/usuario.query'

class UsuarioController{

    //consulta:queryInfo;

    //const sele:string;
    // private respuesta:any={};

    

    constructor(){
    
    }

    public async Usuarios (req:Request,res:Response) {
        let respuesta;
        respuesta = {Text:'Sin Registros'}
        try {
            respuesta = await conexion.query( consul.selectgenral(), {type:QueryTypes.SELECT} )
            console.log(respuesta);
            if(!respuesta[0]  ){
                respuesta = {Text:'Sin Registros'}
            }            
        } catch (error) {
            console.log(error); 
            res.json({Type:'Error', Text:'Texto de error'})
            
        }
        res.json(respuesta)
    }

    public async usuario(req:Request,res:Response){
        let respuesta;
        try{
            respuesta = await conexion.query(consul.selectusuario(req.params.id), {type:QueryTypes.SELECT})
            if(!respuesta[0]){
                respuesta = {Text:'Usuario no encontrado'}
            }
        }catch(error){
            res.json({Type:'Error', Text:'Se presento el siguiente error', Error:error})
        }
        res.json(respuesta)
    }

    public async insertar(req:Request, res:Response){
        let respuesta;
              
        /* console.log(req); */
        
        // res.json(req)
        try{
            respuesta = await conexion.query(consul.insertar(req.body), {type:QueryTypes.INSERT});
            console.log(respuesta);
            
            if(!respuesta[0]){
                respuesta = {Text:'Usuario no encontrado'}  
            }
        }catch(error){
            res.json({Type:'Error', Text:'Se presento el siguiente error', Error:error})
        }
        res.json(respuesta)
    }

    public async Actualizar(req:Request, res:Response){
        let respuesta;
        try{
            respuesta = await conexion.query(consul.actualizar(req.body,req.params.id), {type:QueryTypes.UPDATE});
            if(respuesta.length>0){
                respuesta = {Text:'Actualizado'}
            }else{
                respuesta = {Text:'No se pudo Actualizar'}
            }
        }catch(error){
            res.json({Type:'Error', Text:'Se presento el siguiente error', Error:error})
        }
        res.json(respuesta)  
    }

    public async eliminar(req:Request, res:Response){
        let respuesta;
        try{
            respuesta = await conexion.query(consul.eliminar(req.params.id), {type:QueryTypes.DELETE});
            console.log("Esto es respuesta", respuesta);
            respuesta = {Text:'Usuario eliminado'}
        }catch(error){
            res.json({Type:'Error', Text:'Se presento el siguiente error', Error:error})
        }
        res.json(respuesta)  
    }

}

export const UC = new UsuarioController();